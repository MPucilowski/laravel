<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Psy\Readline\Hoa\Console;
use Symfony\Component\ErrorHandler\Debug;

class ProductController extends Controller
{
    function list():view
    {
        $products = Product::all();
        return view('Test1')->with('products',$products);
    }
    function delete(Product $product):RedirectResponse
    {
        $product->delete();
        return Redirect::route('products.index');
    }

    function create(Request $form):RedirectResponse
    {
        $form->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'price' => 'required|integer|min:0',
            'quantity' => 'required|integer|min:0'
        ]);

        DB::table('products')->insert([
            'name' => $form -> input('name'),
            'desc' => $form -> input('description'),
            'price'=>$form -> input('price'),
            'quantity'=>$form -> input('quantity')
        ]);
        return Redirect::route('products.index');
    }
}
