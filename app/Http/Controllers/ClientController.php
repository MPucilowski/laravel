<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Psy\Readline\Hoa\Console;
use Symfony\Component\ErrorHandler\Debug;

class ClientController extends Controller
{
    function list():view
    {
        $clients = Client::all();

        return view('Clients')->with('clients',$clients);
    }
    function delete(Client $client):RedirectResponse
    {
        $client->delete();
        return Redirect::route('clients.index');
    }

    function create(Request $form):RedirectResponse
    {
        $form->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:clients',
            'gold' => 'required|integer|min:0'
        ]);

        DB::table('clients')->insert([
            'name' => $form -> input('name'),
            'email' => $form -> input('email'),
            'gold'=>$form -> input('gold')
        ]);
        return Redirect::route('clients.index');
    }
}
