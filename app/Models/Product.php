<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'desc', 'quantity', 'price'
    ];

    public function products() : BelongsToMany
    {
        return $this->belongsToMany(client_product::class)->withTimestamps()->withPivot('quantity');
    }
}
