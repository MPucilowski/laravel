<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white overflow-hidden shadow-sm sm:rounded-lg p-6 border-b border-gray-200">
            <form method="POST" action="{!!route('products.create', )!!}">
                @csrf
                <label>Name: </label>
                <br>
                <input name="name" type="text">
                <br>
                <label>Description: </label>
                <br>
                <input name="description" type="text">
                <br>
                <label>Price: </label>
                <br>
                <input name="price" type="number">
                <br>
                <label>Quantity: </label>
                <br>
                <input name="quantity" type="number">
                <br>
                <br>
                <button class="bg-red-600 text-white py-2 px-4 rounded hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-opacity-50" type="submit">Create Item</button>
            </form>
        </div>
    </div>
</x-app-layout>
