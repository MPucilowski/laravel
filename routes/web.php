<?php

use App\Http\Controllers;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ClientProductcontroller;
use App\Http\Controllers\DungeonController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::get('/products', [ProductController::class, 'list'])->name('products.index');
Route::get('/products/{product}/delete', [ProductController::class, 'delete'])->name('products.delete');
Route::get('/create' , function(){return view('Create');})->name('products.form');
Route::post('/create/form' , [ProductController::class, 'create'])->name('products.create');

Route::get('/clients', [ClientController::class, 'list'])->name('clients.index');
Route::get('/clients/{client}/delete', [ClientController::class, 'delete'])->name('clients.delete');
Route::get('/clients/form', function () {return view('Create2');})->name('clients.form');
Route::post('/clients/create', [ClientController::class, 'create'])->name('clients.create');

Route::get('/clientproduct', [ClientProductcontroller::class, 'list'])->name('clientproduct.index');
Route::get('/clientproduct/{client_product}/delete', [ClientProductcontroller::class, 'delete'])->name('clientproduct.delete');
Route::get('/clientproduct/form', [ClientProductcontroller::class, 'insert'])->name('clientproduct.form');
Route::post('/clientproduct/create', [ClientProductcontroller::class, 'create'])->name('clientproduct.create');

Route::get('/dungeon', [DungeonController::class, 'list'])->name('dungeon.index');
Route::get('/dungeon/{dungeon}/delete', [DungeonController::class, 'delete'])->name('dungeon.delete');
Route::get('/dungeon/form', function () {return view('Create4');})->name('dungeon.form');
Route::post('/dungeon/create', [DungeonController::class, 'create'])->name('dungeon.create');
