<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @foreach($dungeons as $Dungeon)
                        <div class="mb-4 p-4 border-b border-gray-200 flex justify-between items-center">
                            <div>
                                <div class="text-lg font-semibold">{{$Dungeon->name}}</div>
                                <div class="text-sm text-gray-600">Floor: {{$Dungeon->floors}}</div>
                                <div class="text-sm text-gray-600">Level: {{$Dungeon->level}}</div>
                            </div>
                            <div>
                                <a href="{{route('dungeon.delete', $Dungeon->id)}}"
                                   class="bg-red-600 text-white py-2 px-4 rounded hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-opacity-50">
                                    Delete
                                </a>
                            </div>
                        </div>
                    @endforeach
                        <a href="{{url('/dungeon/form')}}" class="bg-red-600 text-white py-2 px-4 rounded hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-opacity-50">Create Item</a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
