<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class productSeeder extends Seeder
{

    public function run(): void
    {
        DB::table('products')->insert([
            'name' => 'Healing Potion',
            'desc' => 'Heals 20 HP',
            'price'=>5,
            'quantity'=>1000000
        ]);
        DB::table('products')->insert([
            'name' => 'Draconic Paradise',
            'desc' => 'Smells like Dragon Spirit',
            'price'=>500,
            'quantity'=>1
        ]);
        DB::table('products')->insert([
            'name' => 'Juan',
            'desc' => 'Es Juan',
            'price'=>33,
            'quantity'=>33
        ]);
        DB::table('products')->insert([
            'name' => 'Hola Joan',
            'desc' => 'Ciertamente, Un Producto',
            'price'=>1,
            'quantity'=>10000
        ]);
        DB::table('products')->insert([
            'name' => 'Me Atrapaste',
            'desc' => 'Es cine',
            'price'=>1000000,
            'quantity'=>1000000
        ]);
    }
}
