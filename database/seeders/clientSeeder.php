<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class clientSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('clients')->insert([
            'name' => 'Joan',
            'email' => 'joanqueralt@yahoo.com',
            'gold'=>1000
        ]);
        DB::table('clients')->insert([
            'name' => 'Eloi',
            'email' => 'thedestructor@destroyer.destroy',
            'gold'=>25000
        ]);
        DB::table('clients')->insert([
            'name' => 'Dani',
            'email' => 'Dani@gmail.com',
            'gold'=>3
        ]);
    }
}
