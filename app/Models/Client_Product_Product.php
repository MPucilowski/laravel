<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client_Product_Product extends Model
{
    use HasFactory;
    protected $table = 'client_product_product';
    protected $fillable = [
        'client_product_id', 'product_id', 'quantity'
    ];

}
