<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white overflow-hidden shadow-sm sm:rounded-lg p-6 border-b border-gray-200">
            <form method="POST" action="{!!route('clientproduct.create', )!!}">
                @csrf
                <label>Client: </label>
                <br>
                <select name="client_id" required>
                    @foreach($clients as $cli)
                        <option value="{{ $cli->id }}">{{ $cli->name }}</option>
                    @endforeach
                </select>
                <br>
                <br>
                @foreach($products as $pro)
                    <label>{{$pro -> name}}</label>
                    <br>
                    <label>Quantity: </label>
                    <br>
                    <input name="quantity[{{$pro->id}}][quantity]" type="number" value="0">
                    <br>
                    <br>
                @endforeach
                <br>
                <button class="bg-red-600 text-white py-2 px-4 rounded hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-opacity-50" type="submit">Create Item</button>
            </form>
        </div>
    </div>
</x-app-layout>
