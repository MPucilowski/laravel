<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class dungeonSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('dungeons')->insert([
            'name' => 'Laravel',
            'floors' => 6,
            'level'=>20
        ]);
        DB::table('dungeons')->insert([
            'name' => 'Angular',
            'floors' => 3,
            'level'=>10
        ]);
    }
}
