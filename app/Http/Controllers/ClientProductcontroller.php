<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\client_product;
use App\Models\Client_Product_Product;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Psy\Readline\Hoa\Console;
use Symfony\Component\ErrorHandler\Debug;
use function Laravel\Prompts\select;

class ClientProductcontroller extends Controller
{
    function list():view
    {
        $ClientProduct = Client_Product::with('products', 'client')->get();
        return view('client_product')->with('cp',$ClientProduct);
    }
    function delete(client_product $client_product):RedirectResponse
    {
        $client_product->delete();
        return Redirect::route('clientproduct.index');
    }

    function insert()
    {
        $clients = Client::all();
        $products = Product::all();
        return view('Create3', compact('clients', 'products'));
    }

    function create(Request $form):RedirectResponse
    {
        $form->validate([
            'quantity.*.quantity' => 'required|integer|min:0'
        ]);
        $quantities = $form->input('quantity');

        $invoice = client_product::create([
            'client_id' => $form->input('client_id')
        ]);
        $invoice->save();
        $cont = 0;
        foreach ($quantities as $quanity) {
            $cont++;
            $item = Product::find($cont);

             while ($item == null) {
                 $cont++;
                 $item = Product::find($cont);
             }
                $client = Client::find($form->input('client_id'));
                if (Arr::get($quanity, 'quantity') > 0 && $item->quantity > 0 && (Arr::get($item, 'price') * Arr::get($quanity, 'quantity')) <= $client->gold) {
                    $invoice->products()->attach($cont, [
                        'quantity' => Arr::get($quanity, 'quantity')
                    ]);
                    $item->quantity -= Arr::get($quanity, 'quantity');
                    $item->save();
                    $client->gold -= Arr::get($item, 'price') * Arr::get($quanity, 'quantity');
                    $client->save();
                } else if (($item->quantity == 0 && Arr::get($quanity, 'quantity') > 0) || (Arr::get($item, 'price') * Arr::get($quanity, 'quantity')) > $client->gold) {
                    return Redirect::route('clientproduct.form');
                }

        }
        return Redirect::route('clientproduct.index');
        }
}
