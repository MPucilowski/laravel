<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Dungeon;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Psy\Readline\Hoa\Console;
use Symfony\Component\ErrorHandler\Debug;

class DungeonController extends Controller
{
    function list():view
    {
        $dungeons = Dungeon::all();

        return view('Dungeons')->with('dungeons',$dungeons);
    }
    function delete(Dungeon $dungeon):RedirectResponse
    {
        $dungeon->delete();
        return Redirect::route('dungeon.index');
    }

    function create(Request $form):RedirectResponse
    {
        $form->validate([
            'name' => 'required|string|max:255',
            'floors' => 'required|numeric|min:1',
            'level' => 'required|numeric|min:1'
        ]);

        DB::table('dungeons')->insert([
            'name' => $form -> input('name'),
            'floors' => $form -> input('floors'),
            'level'=>$form -> input('levels')
        ]);
        return Redirect::route('dungeon.index');
    }
}
