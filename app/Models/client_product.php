<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class client_product extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id'
    ];

    public function client() : BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    public function products() : BelongsToMany
    {
        return $this->belongsToMany(Product::class)->withTimestamps()->withPivot('quantity');
    }
}

